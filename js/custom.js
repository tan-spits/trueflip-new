$(window).load(function(){    
    $('.box-graphic-wrap .hide-tab').css({'dispaly':'none'}); 
});
$(function() {
    var ww = $(window).width();
    $('.piggybank-drawdate').each(function () {
       var drawdate = moment($(this).text());
        drawdate.locale('ru');
        $(this).text(drawdate.format('LL в LT'));
    });

    /*--------------- loader ---------------*/
    $(window).load(function() {
        $('body').removeClass('loaded');
    });
    /*--------------- loader ---------------*/

    /*--------------- countdown init ---------------*/
    if ($.fn.countdown) {
        $('.js-countdown').countdown({
            date: '28 december 2016 16:27',
            text: 'Розыгрыш завершен.'
        });
    }
    /*--------------- countdown init ---------------*/

    /*--------------- scroll slider ---------------*/
    if ($('.about-lottery').length) {
        function scrollSlider() {
            var $parent = $('.about-lottery'),
                $parentTop = $parent.offset().top,
                $image = $('.js-about-lottery-slider'),
                $slide = $('.js-about-lottery-slider .item .img-wrap'),
                $text = $('.about-lottery .info'),
                wh = $(window).height(),
                lastScrollTop = 0,
                textInitHeight = $text.outerHeight(),
                headerHeight = $('.header').outerHeight(),
                ww = $(window).width();

            $text.css({
                'min-height': textInitHeight + wh
            });

            $image.css({
               'height': wh - headerHeight
            });

            if (ww < 1023) {
                setTimeout(function () {
                    $text.each(function (el) {
                        var slideHeight = $($text[el]).outerHeight();
                        $($slide[el]).css({
                            'height': slideHeight
                        });
                        console.log('slideHight ' + slideHeight);
                    });
                },100);
            }

            var $parentHeight = $parent.outerHeight();

            var curBlock = 0;

            $(window).scroll(function() {
                var windscroll = $(window).scrollTop();

                if (lastScrollTop > windscroll) {
                    if (windscroll > $parentTop && windscroll < $parentHeight - wh) {
                        $image.css({
                            'top': headerHeight,
                            'position': 'fixed',
                            'bottom': 'auto'
                        })
                    } else if (windscroll < $parentTop) {
                        $image.css({
                            'top': 0,
                            'bottom': 'auto',
                            'position': 'absolute'
                        });
                    } else if (windscroll < $parentTop + $parentHeight - wh) {
                        $image.css({
                            'top': headerHeight,
                            'position': 'fixed',
                            'bottom': 'auto'
                        })
                    }
                    $text.each(function(el) {
                        var blockTop = $($text[el]).offset().top;

                        if (windscroll >= blockTop - wh) {
                            curBlock = el;
                        }
                    });
                } else if (lastScrollTop < windscroll) {
                    if (windscroll > $parentTop && windscroll < $parentHeight - wh) {
                        $image.css({
                            'top': headerHeight,
                            'position': 'fixed',
                            'bottom': 'auto'
                        })
                    } else if (windscroll < $parentTop) {
                        $image.css({
                            'top': 0,
                            'bottom': 'auto',
                            'position': 'absolute'
                        });
                    } else if (windscroll > $parentTop + $parentHeight - wh) {
                        $image.css({
                            'top': 'auto',
                            'bottom': 0,
                            'position': 'absolute'
                        });
                    }
                    $text.each(function(el) {
                        var blockTop = $($text[el]).offset().top;

                        if (windscroll > blockTop - wh) {
                            curBlock = el;
                        }
                    });

                }
                lastScrollTop = windscroll;

                var currentSlide = $lotterySlider.slick('slickCurrentSlide');
                if (currentSlide !== curBlock) {
                    $lotterySlider.slick('slickGoTo', parseInt(curBlock));
                }
            });

            setTimeout(function() {
                $('.slick-dots li').on('click', function() {
                    var index = $('.slick-dots li').index(this);
                    var pos = $($text[index]).offset().top;
                    $('body, html').animate({
                        scrollTop: pos - 180
                    }, 500);
                });
            }, 10);

        }
        scrollSlider();
    }
    /*--------------- scroll slider ---------------*/

    /*--------------- slick init ---------------*/
    if ($.fn.slick) {
        var $lotterySlider = $('.js-about-lottery-slider').slick({
            arrows: false,
            dots: true,
            fade: true,
            speed: 500,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    vertical: true,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    fade: false,
                    draggable: false
                }
            }]
        });

        $('.js-advantages-slider').slick({
            arrows: false,
            dots: true,
            draggable: false,
            rows: 2,
            slidesPerRow: 3,
            responsive: [{
                breakpoint: 640,
                settings: {
                    rows: 1,
                    slidesPerRow: 1
                }
            }]

        });
    }
    /*--------------- slick init ---------------*/

    /*--------------- fancybox init ---------------*/


    if ($('.js-tickets-slider').length) {
        $('.js-tickets-slider').slick({
            arrows: true,
            dots: true,
            draggable: false,
            slidesToShow: 1,
        });
    };

    $(".js-modal-slick-but").on("click", function() {

        /* setTimeout(function(){*/

        $("body").addClass("special-fancy");
        setTimeout(function() {
            if ($('.js-tickets-slider').is('.slick-initialized')) {
                $('.js-tickets-slider').slick('unslick');
            }
            $('.js-tickets-slider').slick({
                arrows: true,
                dots: true,
                draggable: false,
                slidesToShow: 1,
            });
            $(".js-tickets-slider").find(".slick-dots").appendTo(".fancybox-wrap");
            $("#modal-play_3").parents(".fancybox-wrap").append("<a href='#' class='modal-arrow modal-arrow_left js-modal-arrow_left'><span class='mark__cont'><span class='mark-cont__title'>№2</span><span class='mark__icon'></span></span></a><a href='#' class='modal-arrow modal-arrow_right js-modal-arrow_right'><span class='mark__cont'><span class='mark-cont__title'>№4</span><span class='mark__icon'></span></span></a>");
            $('.js-modal-arrow_left').click(function() {
                $(this).parent().find('.slick-prev').click();
                return false;
            });
            $('.js-modal-arrow_right').click(function() {
                $(this).parent().find('.slick-next').click();
                return false;
            });

            $(".js-ticket-del").on('click', function(e) {
                e.preventDefault();
                $(this).parents(".fancybox-skin").append('<span class="box-sure show">Вы действительно хотите полностью удалить этот билет из списка?<span class="sure-buttons"><span class="sure-btn-wrap"><a href="#" class="btn-empty js-remove-slide">да</a></span><span class="sure-btn-wrap"><a href="#" class="btn-empty js-ticket-no">отмена</a></span></span></span>');
                $(".js-ticket-no").on('click', function(e) {
                    e.preventDefault();
                    $(this).parents(".box-sure").detach();
                });
                $(".box-ticket__body").on('click', function(e) {
                    e.preventDefault();
                    $(".box-sure").detach();
                });


            });


            /*------------------ ticket numbers ------------------*/
            /*------------------ ticket numbers ------------------*/
    $(".num-list__item").click(function() {
        var activeItems = $(this).parent().find(".active").length;
        var activeItemsMax = $(this).parent().attr("data-max");
        alert(10);
        if ((activeItems + 1) >= activeItemsMax) {
            $(this).parent().data("ready", true);
        } else {
            $(this).parent().data("ready", false);
        }

        if (activeItems < activeItemsMax) {
            $(this).toggleClass("active");
            return false;
        } else {
            $(this).removeClass("active");
            return false;
        }
    });
    /*------------------ ticket numbers ------------------*/
        }, 100);
        /* */

    });

    /*------------------ ticket counters ------------------*/
    setInterval(function () {
        var ready_count = 0;
        $('.box-ticket').each(function () {
            var ready = false;
            $(this).find('.num-list').each(function (index) {
                if (index == 0) {
                    ready = $(this).data('ready');
                }
                ready &= $(this).data('ready');
            });
            if(ready) {
                ready_count++;
                $(this).data('ready', true);
            }
        });
        $('.price-inf__check-ticket_number').text(ready_count);
        $('.price-inf__summa-number').text((ready_count * 0.001));
    }, 300);
    /*------------------ ticket counters ------------------*/

    if ($.fn.fancybox) {
        $('.fancybox').fancybox({
            padding: 0,
            helpers: {
                overlay: {
                    locked: false
                }
            },
            tpl: {
                closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close"></div>'
            },
            afterShow: function() {
                if ($('.js-menu-wrap').hasClass('open')) {
                    $('.js-menu-wrap').fadeOut().removeClass('open');
                    $('body').removeClass('body-overflow');
                }
            },
            afterClose: function() {
                $('body').removeClass("special-fancy");

            },
            margin: 10,
            fixed: true
        });
        $('.fancybox2').fancybox({
            padding: 0,
            helpers: {
                overlay: {
                    locked: false
                }
            },
            tpl: {
                closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close"></div>'
            },
            afterShow: function() {
                $('body').addClass("special-fancy");
            },
            afterClose: function() {
                $('body').removeClass("special-fancy");

            },
            margin: 10,
            fixed: true
        });
    }
    /*--------------- fancybox init ---------------*/

    /*--------------- change modal content ---------------*/
    $('.js-modal-change').on('click', function(e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $('.js-modal-in').removeClass('active');
        $(target).addClass('active');

        $("body").addClass("special-fancy");



    });

    /*--------------- custom-scroll init ---------------*/
    if ($.fn.mCustomScrollbar) {
        if (ww >= 640) {
            $('.js-custom-scroll').mCustomScrollbar({
                theme: "orange"
            });
        }
    }

    /*--------------- accordion ---------------*/
    $('.js-accordion-link').on('click', function(e) {
        e.preventDefault();

        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).next().slideUp();
            return false;
        } else {
            $(this).addClass('open');
            $(this).next().slideDown();
        }
    });
    /*--------------- accordion ---------------*/



    /*---------------fixed aside-------------------*/
    if ($('.js-wrapper-fix').length) {

        var leftTop = $('.js-wrapper-fix'),
            scrollBlock = $('.js-aside-fix');

        function stikyLeftSide(rightSide, leftCol) {
            var scrollPos = $(window).scrollTop();
            var leftTop = leftCol.offset().top;
            var width = $('.box-fixed').width();

            var rightTop = $(".js-static").outerHeight();
            var asideHeight = $('.js-aside-fix').outerHeight(),
                headerHeight = $('header').outerHeight(),
                asideCont = $('.profile-cont_center').outerHeight() - headerHeight;
            if (scrollPos > (leftTop + rightTop) && scrollPos < (leftTop + leftCol.height() - rightSide.outerHeight() - headerHeight)) {
                rightSide.addClass('fixed').css('width', width).removeClass('bottom');
                //rightSide.removeClass('top');
            } else if (scrollPos < (leftTop + rightTop)) {
                rightSide.removeClass('fixed').css('width', '100%');
            } else if (scrollPos > leftTop && (scrollPos) > (leftTop + leftCol.height() - rightSide.outerHeight() - headerHeight)) {
                rightSide.removeClass('fixed').addClass('bottom');
            }
        }
        stikyLeftSide(scrollBlock, leftTop);
        $(window).on('scroll', function() {
            stikyLeftSide(scrollBlock, leftTop);
        });
        $(window).resize(function() {
            stikyLeftSide(scrollBlock, leftTop);
        });
    };
    /*---------------fixed aside-------------------*/

    /*--------------- add class to header with scroll ---------------*/
    $(window).scroll(function() {
        var windscroll = $(window).scrollTop();
        if (windscroll > 0) {
            $('.header').addClass('scroll');
        } else {
            $('.header').removeClass('scroll');
        }
    });
    /*--------------- add class to header with scroll ---------------*/

    /*--------------- set device height to block ---------------*/
    function setSize() {
        var winHeight = $(window).height(),
            headerHeight = $('.header').outerHeight(),
            footerHeight = $('.footer').outerHeight(),
            contentHeight = winHeight - headerHeight - footerHeight;
        $('.js-content').css({
            'min-height': contentHeight,
            'padding-top': headerHeight,
            'padding-bottom': footerHeight
        });
        $('.content-wrapper').css({
            'min-height': winHeight
        })
    }
    setSize();
    $(window).resize(setSize);
    /*--------------- set device height to block ---------------*/

    /*--------------- open menu ---------------*/
    $('body').bind('click', function(e) {
        if (/ip(hone|od)|ipad/i.test(navigator.userAgent)) {
            $('body').css("cursor", "pointer");
        }
        if ($(e.target).closest('.js-menu-btn').length !== 0) {
            $('.js-menu-wrap').fadeIn().addClass('open');
            $('body').addClass('body-overflow');
        } else if ($(e.target).closest('.js-close-menu').length !== 0) {
            $('.js-menu-wrap').fadeOut().removeClass('open');
            $('body').removeClass('body-overflow');
        } else if ($(e.target).closest('.js-menu').length === 0) {
            $('.js-menu-wrap').fadeOut().removeClass('open');
            $('body').removeClass('body-overflow');
        }
    });
    /*--------------- open menu ---------------*/


    /*--------------- open menu in page profile---------------*/
    $('.js-menu-profile-btn').click(function() {
        $('.menu-wrap').addClass('menu-profile-open');
        $('html').addClass('body-overflow');
    });

    $('.close-menu').click(function() {
        $('.menu-wrap').removeClass('menu-profile-open');
        $('html').removeClass('body-overflow');
    });
    $('.menu-wrap').click(function() {
        $('.menu-wrap').removeClass('menu-profile-open')
        $('html').removeClass('body-overflow');
    });
    /*--------------- open menu in page profile ---------------*/


    /*--------------- hide block ticket ihf ---------------*/
    var box_profile_height = $('.box-profile-ticket').height();
    if ($('.box-profile-ticket').length) {
        if (box_profile_height <= 300) {
            $(".price-inf__bottom").addClass("jd");
        }
    };

    var handler = function() {

    }
    $(window).bind('load', handler);
    $(window).bind('resize', handler);
    /*--------------- hide block ticket ihf ---------------*/



    /*--------------- show more langs ---------------*/
    $('body').on('click', function(e) {
        if ($(e.target).closest('.js-lang-more').length === 0) {
            $('.js-lang-sublist').fadeOut();
        } else if ($(e.target).closest('.js-lang-sublist li').length !== 0) {
            $('.js-lang-sublist').fadeOut();
        } else if ($(e.target).closest('.js-lang-more').length !== 0) {
            $('.js-lang-sublist').fadeToggle();
        }
    });
    /*--------------- show more langs ---------------*/

    /*--------------- mobile functions ---------------*/
    $(document).on('touchstart', function() {
        documentClick = true;
    });
    $(document).on('touchmove', function() {
        documentClick = false;
    });
    /*--------------- mobile functions ---------------*/

    $(document).on('click', function(event) {
        if (event.type == "click") documentClick = true;
        if (documentClick) {
            var target = $(event.target);

            /*--------------- show more langs ---------------*/
            if (target.is('.js-select-lang-btn')) {
                event.preventDefault();
                $('.js-lang-list').fadeToggle();
            } else {
                $('.js-lang-list.mob').fadeOut();
            }
            /*--------------- show more langs ---------------*/

            /*--------------- select lang ---------------*/
            if (target.is('.js-lang.mob')) {
                var lang = $(target).attr('data-lang'),
                    text = $(target).text();
                $('.js-select-lang-btn').attr('data-lang', lang).text(text);
                $('.js-lang-list').fadeOut();
            }
            /*--------------- select lang ---------------*/
        }
    });
    /*--------------- open steps in mob ---------------*/
    $('.js-more-link').on('click', function(e) {
        $('.js-more-block-wrap').find('.js-hide-mob').fadeIn();
        $(this).hide();
        return false;
    });
    /*--------------- open steps in mob ---------------*/

    /*--------------- open usermenu ---------------*/
    $('.js-user').on('click', function(e) {
        $(this).toggleClass('open-usermenu');
        $('.usermenu').slideToggle();
        $('.hide-popup').toggleClass('visibl');
        $('.balance-dropdown').removeClass('open-dropdown');
        return false;
    });

    /*--------------- open usermenu ---------------*/

    /*--------------- hide usermenu ---------------*/
    $(".hide-popup").click(function() {
        $('.js-user').removeClass('open-usermenu');
        $('.usermenu').slideUp();
        $('.js-balance').removeClass('open-balance');
        $('.balance-dropdown').removeClass('open-dropdown');
        $(this).removeClass('visibl');
    });
    /*--------------- hide usermenu ---------------*/

    /*--------------- hidden ticket ---------------*/
    $('.js-ticket-hidden').on('click', function(e) {
        $(this).parents('.profile-ticket__item').addClass('ticket-hidden');
        return false;
    });
    /*--------------- hidden ticket ---------------*/

    /*--------------- hidden active in ticket ---------------*/
    $('.js-ticket-clear').on('click', function(e) {
        $(this).parents('.profile-ticket__item').find('.num-list__item').removeClass('active');
        return false;
    });

    /*--------------- hidden active in ticket ---------------*/

    /*--------------- open balance-dropdown ---------------*/
    $('.js-balance').on('click', function(e) {
        $(this).toggleClass('open-balance');
        $('.balance-dropdown').toggleClass('open-dropdown');
        $('.js-user').removeClass('open-usermenu');
        $('.hide-popup').toggleClass('visibl');
        $('.usermenu').slideUp();
        return false;
    });
    /*--------------- open balance-dropdown ---------------*/

    /*--------------- show video ---------------*/
    $('.js-modal-video').click(function() {
        var srcVideo = $(this).attr('data-src');
        $(this).parent().addClass("hide-video");
        $(this).parent().find('iframe').attr('src', srcVideo);
        return false;
    });
    /*--------------- show video ---------------*/

    /*--------------- fancybox with video ---------------*/
    if ($('.fancybox-video').length) {
        $('.fancybox-video').fancybox({
            padding: 0,
            wrapCSS: "styled_close",
            helpers: {
                overlay: {
                    locked: false
                }
            },
            tpl: {
                closeBtn: '<div title="Закрыть" class="fancybox-item fancybox-close"></div>'
            },
            beforeClose: function() {
                $('.modal-timeline-video').find('iframe').removeAttr('src');
                $('.modal-timeline-video').removeClass('hide-video');
            },
            beforeShow: function(){
                $("body").css({'overflow-y':'hidden'});
            },
            afterClose: function(){
                $("body").css({'overflow-y':'visible'});
            },
            margin: 0
        });
    }
    /*--------------- fancybox with video ---------------*/


    /*--------------- tabs in transactions ---------------*/
    $('.js-switcher').change(function() {
        var el = $(this),
            parents = el.parents('.switcher'),
            id;
        parents.toggleClass('off');
        if (el.is(':checked')) {
            id = parents.find('.f_off').attr('data-href');

        } else {
            id = parents.find('.f_on').attr('data-href');
        }
        $('.js-switcher-tab').removeClass('active');
        $('#' + id + '').addClass('active');
        return false;
    });
    /*--------------- tabs in transactions ---------------*/
    $(".mark__item").addClass('hide');
    $(".modal-play_4 .mark__item").removeClass('hide');
    $(".js-tickets-num").click(function() {
        $(".js-tickets-num").removeClass("active");
        $(this).toggleClass("active");
        $(".box-mark").addClass("show");
        $('.js-mark-item').addClass("hide");
        var tickets_num = $(this).text();
        var ticket = $(".js-mark-item");

        for (i = 0; i < tickets_num; i++) {
            /*$(".js-mark-item")[i].removeClass('hide');*/

            ticket.eq(i).removeClass('hide');

        };
    });

    /*------------------ ticket numbers ------------------*/
    $(".num-list__item").click(function() {
        var activeItems = $(this).parent().find(".active").length;
        var activeItemsMax = $(this).parent().attr("data-max");
        if ((activeItems + 1) >= activeItemsMax) {
            $(this).parent().data("ready", true);
        } else {
            $(this).parent().data("ready", false);
        }

        if (activeItems < activeItemsMax) {
            $(this).toggleClass("active");
            return false;
        } else {
            $(this).removeClass("active");
            return false;
        }
    });
    /*------------------ ticket numbers ------------------*/



    /*----------------------- tabs -----------------------*/
    $('.tabs li a').click(function() {
        $(this).parents('.tab-wrap').find('.tab-cont').addClass('hide-tab');
        $(this).parent().siblings().removeClass('active');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        $(this).parent().addClass('active');
        return false;
    });
    /*----------------------- tabs -----------------------*/

    $(".js-update-modal").click(function() {
        $.fancybox.update();
    });

    /*---------------- custom select ---------------------*/
    if ($('.styled').length) {
        $('.styled').styler();
    };
    /*---------------- custom select ---------------------*/
    /*---------------- periodpicker ---------------------*/
    if ($('#periodpickerstart').length) {
        $('#periodpickerstart').periodpicker({
            end: '#periodpickerend',
            lang:'ru',
            yearsLine: false,
            resizeButton: false,
            fullsizeButton: false,
            closeButton: false,
            formatDate: 'DD/MM/YYYY'
        });
    };
    /*---------------- periodpicker ---------------------*/
    /*---------------hidden-rofile-item------------------*/
    $('.js-hidden-rofile-item').click(function() {
        $(this).parents('.box-profile-item').addClass('hidden-rofile-item');
        return false;
    });
    /*--------------show-select --------------------*/ 
     $('.js-show-select').click(function() {
        $(this).parents('.list-check__item').toggleClass('show-select');
        $(this).parent().siblings().removeClass('show-select');
        return false;
    });
    /*--------------show-select --------------------*/ 

    /*--------------tabs page investor------------------*/
    $('.tabs li .js-inves__link').click(function() {
        var id = $(this).attr('data-href');
        $(id).removeClass('hidden-rofile-item');
        $('html, body').animate({scrollTop: $(id).offset().top - 65}, 1000);
        return false;
    });
    /*---------------tabs page investor ------------------*/
    /*--------------tabs winners------------------*/
    $('.winners-list li a').click(function() {
        $(this).parents('.box-winners-list__row').find('.winners-history').addClass('hide-tab');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        $(id).find('.profile-tab-item').removeClass('hidden-rofile-item');
        $('html, body').animate({scrollTop: $(id).offset().top - 65}, 1000);
        return false;
    });
    /*---------------tabs winners ------------------*/

     /*----------------------- tabs achive-----------------------*/
    $('.archive-table-title__left a').click(function() {
        $('.archive-tab__link').addClass('active');
        $('.archive-table-title__right').addClass('hide-tab');
        $(this).parents('.archive-table-title').addClass('active-tab');
        $('.archive-table').addClass('hide-tab');
        var id = $(this).attr('data-href');
        $(id).removeClass('hide-tab');
        $(this).removeClass('active');
        return false;
    });
    /*----------------------- tabs archive-----------------------*/
    /*--------------- archive-check ------------------*/
    $('.js-archive-check').click(function() {
        $(this).parent().toggleClass('active');
        return false;
    });
    /*-------------- archive-check --------------------*/

    /*---------------hidden-winners-history------------------*/
    $('.js-hidden-rofile-item').click(function() {
        $(this).parents('.winners-history').addClass('hide-tab');
        return false;
    });
    /*--------------hidden-winners-history --------------------*/
    /*---------------counter investor ------------------*/
    $('.counter__link-prev').click(function () {
         var $input = $(this).parents('.box-counter').find('input');
         var count = parseInt($input.val()) - 1;
         count = count < 1 ? 1 : count;
         $input.val(count);
         $input.change();
         return false;
     });
     $('.counter__link-next').click(function () {
         var $input = $(this).parents('.box-counter').find('input');
         var count = parseInt($input.val()) + 1;
         count = count > ($input.attr("maxlength")) ? ($input.attr("maxlength")) : count;
         $input.val(count);
         $input.change();
         return false;
     });
    /*---------------counter investor ------------------*/
    /*---------------hidden-block-video------------------*/
    $('.js-hidden-video').click(function() {
        $(this).parents('.box-inves-video').hide();
         $(this).parents('.tab-wrap-with-video').find('.nav-tab-list-wrap').addClass('nav-tab-list-wrap-no-vid');
        return false;
    });
    /*--------------hidden-block-video --------------------*/

    /*--------------hidden-block-video --------------------*/ 
    /*--------------graphic invest --------------------*/
    if ($('#graphic').length) {
        $('#graphic').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 20
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: ['ДЕК 16', 'ЯНВ 17', 'ФЕВ 17', 'МАР 17', 'АПР 17', 'МАЙ 17',
                'ИЮН 17', 'ИЮЛ 17', 'АВГ 17', 'СЕН 17', 'ОКТ 17', 'НОЯ 17', ]
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Ваш доход, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '16px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999',
                        fontSize: '14px'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0,
                tickInterval:2
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999',
                        fontSize: '14px'
                    }
                },
                min:0,
                tickInterval:250000,
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: [200000, 510000, 600000, 900000, 760000, 1200000, 1010000, 2200000, 1300000, 800000,820000, 1200000, ]

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: [0.5, 2, 2.2, 2.5, 2.6, 4, 4.2, 4.5, 4.6, 4.6,4.8, 8.1, ]
            }
            ]
        });
    };
    if ($('#graphic2').length) {

        $('#graphic2').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 40
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: ['ДЕК 16', 'ЯНВ 17', 'ФЕВ 17', 'МАР 17', 'АПР 17', 'МАЙ 17',
                'ИЮН 17', 'ИЮЛ 17', 'АВГ 17', 'СЕН 17', 'ОКТ 17', 'НОЯ 17', ]
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Ваш доход, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0,
                tickInterval:2
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999'
                    }
                },
                min:0,
                tickInterval:250000,
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: [500000, 110000, 900000, 200000, 900000, 1200000, 1510000, 1200000, 1900000, 600000,220000, 6200000, ]

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: [6.5, 5, 6.2, 7.5, 2.6, 5, 6.2, 3.5, 5.6, 3.6,8.8, 8.1, ]
            }
            ]
        });
    };
    if ($('#graphic3').length) {
        $('#graphic3').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 40
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: ['ДЕК 16', 'ЯНВ 17', 'ФЕВ 17', 'МАР 17', 'АПР 17', 'МАЙ 17',
                'ИЮН 17', 'ИЮЛ 17', 'АВГ 17', 'СЕН 17', 'ОКТ 17', 'НОЯ 17', ]
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Ваш доход, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0,
                tickInterval:2
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999'
                    }
                },
                min:0,
                tickInterval:250000,
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: [500000, 110000, 600000, 400000, 200000, 1200000, 1510000, 1200000, 1900000, 600000,220000, 6200000, ]

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: [6.5, 1, 6.2, 7.5, 2.6, 5, 5.2, 3.5, 2.6, 3.6,8.8, 1.1, ]
            }
            ]
        });
    };
    if ($('#graphic4').length) {
        $('#graphic4').highcharts({
            chart: {
                renderTo: 'graphic',
                zoomType: 'xy',
                type: 'spline',
                backgroundColor: 'transparent',
                marginTop: 40
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
                labels:{
                    rotation:0,
                }, 
                tickColor: 'tranasparent',             
                categories: ['ДЕК 16', 'ЯНВ 17', 'ФЕВ 17', 'МАР 17', 'АПР 17', 'МАЙ 17',
                'ИЮН 17', 'ИЮЛ 17', 'АВГ 17', 'СЕН 17', 'ОКТ 17', 'НОЯ 17', ]
            }],
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
                yAxis: [{ // Primary yAxis
                   title: {
                    text: 'Ваш доход, USD',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                    y: 0,
                    x: 0,
                    offset: 12
                },
                labels: {
                    style: {
                        color: '#999'
                    }
                },
                title: {
                    text: ' ',
                    style: {
                        color: '#000'
                    }
                },
                opposite: true,
                showEmpty: false,
                min:0,
                tickInterval:2
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        fontFamily: 'MuseoSansCyrl;',
                        fontSize: '14px',
                        fontWeight: '700',
                        color: '#000'
                    },
                    align:'high',
                    rotation:0,
                },
                gridLineColor: '#eeeeee',
                labels: {
                    formatter: function() {
                        return this.value +'  ';
                    },
                    style: {
                        color: '#999'
                    }
                },
                min:0,
                tickInterval:250000,
                showEmpty: false

            }, { // Tertiary yAxis
                title: {
                    text: ' ',
                    style: {
                        color: '#999'
                    }
                }
            }],
            tooltip: {
                enabled: false
            },
            plotOptions: {
                lineWidth: 2,
                series: {
                    shadow: false
                },          
                spline: {
                    lineWidth: 2,
                    borderRadius: 0,
                    shadow : false,
                    marker: {
                        enabled: false
                    },
                    column: {
                        borderWidth: 0,
                        shadow: false
                    }
                }
            },
            series: [{
                name: '2013',
                color: '#dcd3ec',
                type: 'column',
                yAxis: 1,
                borderWidth: 0,                             
                data: [500000, 110000, 200000, 200000, 200000, 1200000, 1510000, 1200000, 1900000, 200000,220000, 6200000, ]

            },
            {
                name: '2015',
                color: '#5d4c84',
                type: 'spline',            
                data: [1.5, 2, 6.2, 5.5, 8.6, 5, 6.2, 3.5, 5.6, 5.6,8.8, 8.1, ]
            }
            ]
        });
    };

    /*--------------graphic invest --------------------*/
    /*-------------- profile-table- combination --------------------*/
    $(".js-more-comb").click(function() {
        $(this).parent().find(".js-more-comb-list").toggleClass('comb-visib');
        $(this).toggleClass('active');
        $(this).parents('.profile-table-list__item').toggleClass('comb-visib');
        
        return false;
    });
    /*-------------- profile-table- combination --------------------*/

    $(".js-chesk").click(function() {
        $(this).parent().find(".js-check-num").slideToggle();
        return false;
    });

    /*--------------marketing plan show details --------------------*/
    $('.js-marketing-plan-link').on('click', function (e) {
        e.preventDefault();

        var index = $(this).parent().parent().index(),
            block = $('.js-marketing-plan-details').not('.mob');

        var blockToShow = $(".js-marketing-plan-details[data-id='" + index +"']").not('.mob');

        if($(blockToShow).hasClass('active')){
            $(blockToShow).removeClass('active').slideUp();
            $(this).removeClass('open');
        } else {
            $(block).slideUp().removeClass('active');
            $(blockToShow).addClass('active').slideDown();
            $(this).addClass('open');
        }
    });

    function deleteClassMob(){
        var winWidth = $(window).width();

        if (winWidth < 640) {
            if ($('.js-marketing-plan-details').hasClass('mob')) {
                $('.js-marketing-plan-details').removeClass('mob').addClass('mob-active');
            }
        } else if (winWidth >= 640) {
            if ($('.js-marketing-plan-details').hasClass('mob-active')) {
                $('.js-marketing-plan-details').removeClass('mob-active').addClass('mob');
            }
        }
    }
    deleteClassMob();
    $(window).resize(deleteClassMob);
    /*-------------- marketing plan show details --------------------*/

    if($.fn.slider){
        /*-------------- offering-range-slider --------------------*/
        $("#offering-range-slider").slider({
            ticks: [0, 25000, 50000, 75000, 100000],
            ticks_labels: ['0', '25 000', '50 000', '75 000', '100 000'],
            ticks_snap_bounds: 30,
            tooltip: 'hide',
            value: 65000
        });
        /*-------------- offering-range-slider --------------------*/
        /*-------------- profit-range-slider --------------------*/
        var profits = {
            0: {
                'monthly expences': '$ 558 000',
                'number tickets month': '279',
                'revenue': '$ 99 324',
                'monthly profit': '$ 0.10',
                'tockens': '10',
                'year profit': '$ 60.30'
            },
            500000: {
                'monthly expences': '$ 279 000',
                'number tickets month': '1 395 000',
                'revenue': '$ 502 200',
                'monthly profit': '$ 0.50',
                'tockens': '10',
                'year profit': '$ 60.30'
            },
            1000000: {
                'monthly expences': '$ 5 580 000',
                'number tickets month': '2 790 000',
                'revenue': '$ 993 240',
                'monthly profit': '$ 0.99',
                'tockens': '10',
                'year profit': '$ 60.30'
            }
        };
        $("#profit-range-slider").slider({
            ticks: [0, 500000, 1000000],
            ticks_labels: ['100 000', '500 000', '1 000 000'],
            ticks_snap_bounds: 30,
            tooltip: 'hide',
            value: 500000,
            step: 500000
        }).on('slideStop', function (slideEvt) {
            var currrentVal = slideEvt.value;

            setCalcResults(currrentVal);
        });
        $('#js-profit-select-styler li').click(function () {

            var selectVal = $('#js-profit-select').val();

            setCalcResults(selectVal);
        });
        function getInitValue(){
            var initValue = $("#profit-range-slider").slider('getValue');

            setCalcResults(initValue);
        }
        getInitValue();
        function setCalcResults(val){
            $('.js-monthly-expences').text(profits[val]["monthly expences"]);
            $('.js-number-tickets-month').text(profits[val]["number tickets month"]);
            $('.js-revenue').text(profits[val]["revenue"]);
            $('.js-monthly-profit').text(profits[val]["monthly profit"]);
            $('.js-tockens').text(profits[val]["tockens"]);
            $('.js-year-profit').text(profits[val]["year profit"]);
        }
        /*-------------- profit-range-slider --------------------*/
    }

    function get_ticket(selector) {
        var ticket = {};
        selector.find(".num-list__item.active").each(function (index) {
            ticket['n' + (index + 1)] = $(this).text();
            ticket['address'] = $('.balance-dropdown__text').eq(0).text();
            ticket['payout_address'] = $('.balance-dropdown__text').eq(0).text();
            ticket['amount'] = 0.001;
        });
        return ticket;
    }
    $('.btn-play-now').click(function () {
        var tickets = [];
        $('.box-ticket').each(function () {
            if($(this).data('ready')) {
                tickets.push(get_ticket($(this)));
            }
        });
        console.log(tickets);
        var btn = $(this);
        $.ajax({
            url: "/lotto/tickets",
            method: "POST",
            data: JSON.stringify(tickets),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                btn.prop('disabled', true);
            },
            success: function () {
                alert("Успех");
                window.location.href = "/profile";
            }
        });
        //$('#logout_form').attr('method', 'POST').attr('action', '/lotto/tickets').submit();
        return false;
    });
    $('.btn-reserve').click(function () {
        var tickets = [];
        $('.box-ticket').each(function () {
            if($(this).data('ready')) {
                tickets.push(get_ticket($(this)));
            }
        });
        var btn = $(this);
        console.log(tickets);
        $.ajax({
            url: "/lotto/tickets/reserved",
            method: "POST",
            data: JSON.stringify(tickets),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                btn.prop('disabled', true);
            },
            success: function () {
                alert("Успех");
                window.location.href = "/profile";
            }
        });
        //$('#logout_form').attr('method', 'POST').attr('action', '/lotto/tickets').submit();
        return false;
    });
    /*----------------------- ticket buy buttons -----------------------*/

    $(".js-update-modal").click(function() {
        $.fancybox.update();
    });

    /*---------------- custom select ---------------------*/
    if ($('.styled').length) {
        $('.styled').styler();
    };
    /*---------------- custom select ---------------------*/

    $(".js-chesk").click(function() {
        $(this).parent().find(".js-check-num").slideToggle();
        return false;
    });

    /* AJAX forms */
    $('#register_form').submit(function () {
        var form = $(this);
        var error = false;
        form.find('input, textarea').each( function(){ // прoбeжим пo кaждoму пoлю в фoрмe
            if ($(this).val() == '') { // eсли нaхoдим пустoe
                alert('Зaпoлнитe пoлe "'+$(this).attr('placeholder')+'"!'); // гoвoрим зaпoлняй!
                error = true; // oшибкa
            }
        });
        if (!error) {
            var data = form.serialize();
            $.ajax({
                type: 'POST',
                url: '/signup',
                dataType: 'json',
                data: data,
                beforeSend: function(data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success: function(data){
                    console.log(data);
                    localStorage.setItem('api_key', data.api_key);
                    window.location.href = '/profile/';
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
                    alert("Ошибка");
                },
                complete: function(data) { // сoбытиe пoслe любoгo исхoдa
                    form.find('input[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
                }

            });
        }
        return false;
    });


    $('#login_form').submit(function () {
        var form = $(this);
        var error = false;
        form.find('input, textarea').each( function(){ // прoбeжим пo кaждoму пoлю в фoрмe
            if ($(this).val() == '') { // eсли нaхoдим пустoe
                alert('Зaпoлнитe пoлe "'+$(this).attr('placeholder')+'"!'); // гoвoрим зaпoлняй!
                error = true; // oшибкa
            }
        });
        if (!error) {
            var data = form.serialize();
            $.ajax({
                type: 'POST',
                url: '/login',
                dataType: 'json',
                data: data,
                beforeSend: function(data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success: function(data){
                    console.log(data);
                    localStorage.setItem('api_key', data.api_key);
                    window.location.href = '/profile/';
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
                    alert("Ошибка");
                },
                complete: function(data) { // сoбытиe пoслe любoгo исхoдa
                    form.find('input[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
                }

            });
        }
        return false;
    });

    $('.js-logout').click(function () {
        $('#logout_form').submit();
        return false;
    });



    /* Transactions */
    $('.period_picker_show ').click(function () {
       $.ajax({
          url: "/bank/" 
       });
    });
});

var handler = function() {
    var height_win = $(window).height();
        
    

   
       
    

};

$(window).bind('load', handler);
$(window).bind('resize', handler);